# Breast Cancer 

link: 
https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Coimbra#

Abstract: Clinical features were observed or measured for 64 patients with breast cancer and 52 healthy controls.

### Information:
 There are 10 predictors, all quantitative, and a binary dependent variable, indicating the presence or absence of breast cancer.
The predictors are anthropometric data and parameters which can be gathered in routine blood analysis.
Prediction models based on these predictors, if accurate, can potentially be used as a biomarker of breast cancer.


Associated Tasks: Classification

Attribute Characteristics:Integer

Data Set Characteristics: Multivariate

Number of Instances:116

Number of Attributes:10

### Attribute Information:

#### Quantitative Attributes:

Age (years) 
BMI (kg/m2)
Glucose (mg/dL)
Insulin (nU/mL)
HOMA
Leptin (ng/mL)
Adiponectin (ng/mL)
Resistin (ng/mL)
MCP-1(pg/dL)

#### Labels:
1=Healthy controls

2=Patients

